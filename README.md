
This is the version of LIFE that I wrote in 1986.
I included it on a disk of freeware along with SUPERSAVE.
So it probably didn't get seen widely.

I modified it today (4/15/20), adding John Conway's name. 

I also added REDO, BACK, AID keys. 
I inserted Tursi's LOADCHAR routine for the cartridge version. 
Finally, I put in an Easter Egg.

The ZIP has the individual files in FIAD.
The DSK file is for Classic99 (V9T9).


About the DSK file:

This is a V9T9 format disk. Most emulators like Classic99, V9T9, js99er.net accept V9T9 disks.

Using the DSK file in your emulator, there are 3 ways to run it:

E/A 3 Load and Run: filename LIFEO program name START
E/A 5 Run Program File: filename LIFE
Cartridge LIFE_C  - load into Classic99, js99er.net, or real cart. It doesn't require 32K.

About the ZIP file:

The zip file contains all the sources, plus LIFEO and LIFE in TIFILES format. Extract
the zip to a folder. In Classic99, mount the folder as FIAD (files in a disk.) Then
instead of LIFEO use DSK1.LIFEO.TFI . Similarly the program file is LIFE.TFI .

The TFI files are necessary, Otherwise no program would know the internal
format of LIFEO or LIFE.

Rebuilding

The Makefile uses Ralph Benziger's XDT99 package which is written in Python.
Install XDT99, and edit the Makefile to point XAS and XDM at your copies.

-Erik Olson
