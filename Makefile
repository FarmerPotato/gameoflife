all: LIFEO LIFE LIFE_C.bin

dist: tfis disk zip

tfis: LIFE.tfi LIFEO.tfi

clean: 
	rm -f $(PRODUCTS) $(CART) $(LISTS) 

# Outputs
PRODUCTS=LIFEO LIFE $(TFIS)
CART=LIFE_C.bin
LISTS=LIFEL LIFEIMGL LIFECARTL
ZIP=GAMEOFLIFE.ZIP
DSK=GAMEOFLIFE.DSK
TFIS=LIFEO.tfi LIFE.tfi

# Rools
PYTHON=/usr/bin/python
XAS=/cygdrive/c/TI99/xdt99/xas99.py
XDM= /cygdrive/c/TI99/xdt99/xdm99.py

# Rules
LIFEO: LIFES
	# convert to TIFILES
	$(PYTHON) $(XAS) -R LIFES -L LIFEL -o $@ 

LIFEO.tfi: LIFEO
	# convert to TIFILES
	$(PYTHON) $(XDM) -f DF80 -T $<

LIFE: LIFES LIFEIMGS
	$(PYTHON) $(XAS) -R LIFEIMGS -L LIFEIMGL -o LIFE --image 

LIFE.tfi: LIFE
	$(PYTHON) $(XDM) -f PGM -T $<

LIFE_C.bin: LIFES LIFECARTS LOADCHARS
	$(PYTHON) $(XAS) -R LIFECARTS -L LIFECARTL --binary -o $@
	mv LIFE_C.bin_6000 $(CART)

SRCS=LIFES LIFEIMGS LIFECARTS LOADCHARS
FILES=$(TFIS) $(SRCS)

disk: $(FILES)
	$(PYTHON) $(XDM) -X 360 $(DSK) 
	$(PYTHON) $(XDM) $(DSK) -f DF80 -a LIFEO
	$(PYTHON) $(XDM) $(DSK) -f DV80 -a $(SRCS)
	$(PYTHON) $(XDM) $(DSK) -f PGM -a LIFE 


zip: $(ZIP)

$(ZIP): $(FILES) $(CART) Makefile
	zip $(ZIP) $(FILES) $(CART)  Makefile

